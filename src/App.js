import React from 'react';
import CardUsers from './component/CardUsers'
import Header from './component/Header'

function App() {
  return (
    <div className="App">
      <Header />
      <CardUsers />
      </div>
  );
}

export default App;
