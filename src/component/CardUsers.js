import React , {useState , useEffect , Fragment} from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.css';
import '../App.css';

export default function CardUsers () {


    //init state , setState
    const [dataUsers , setData] = useState([])
    const urlImg = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBhUIBwgWFhUWGBsZGRgYGRseHRwgISEdJSAmIyEfKCghJSApIiIiITYiJSkrOi4uICI6ODUwNyguLisBCgoKDg0OGxAQGjUlICUwLzc1NzAuMi0rLi8tLS8tNTctLzUrLS8tLi8tLS8wNy0tLTUtKy01NS8tLS0tMjUuLf/AABEIAKgBLAMBIgACEQEDEQH/xAAaAAEAAgMBAAAAAAAAAAAAAAAABQYCAwQB/8QANhAAAgEDAwIFAwMCBQUBAAAAAAECAwQRBRIhEzEiQVFTkQZhcRQygSNCFVKhsfCzwdHh8TP/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIDBQT/xAAgEQEAAQUAAwADAAAAAAAAAAAAAQIREhNRITFBYfDx/9oADAMBAAIRAxEAPwCggA9Z4oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9pwlUqKnTWW2kl932Ju4sNEt7l2Na+qqpF7ZVNsXT3LvwvHhPjJwaPbXdxfwlZ20puEoye1dsNPl9l28yc1H6aq3OvVs39GKc51GnPxKDeW3FLjh+bRqIcq6oibXRy+nbyNdq4nGFOKUnWbzT2vs4tfub8kufwZUtP0m+lK2065rOqoylFzjFQntWWkl4k2k8ZJyrLSLm3elvU6Kt0kqSxLfGfPjbaSbk28rth/PNon0/dWGt7pXFGcqUZtwhPMsuDUfC0ny2ufuLMZzbzNlS7g2XFrcWVToXdCUJLykmn/8APuazLuAltG020u7KteX9zKEaWz9sVJve2v8AfBv/AMH0+8tqlTR9QlOdOLnKE4bW4ru0/t6FszNcXsggexi5vEFn8HhGwHu2WzftePXyPFy8IADKNOTqqk1htpc/c3ajZ1NPvp2daSbg8NrOP9SpdzgAigAAAAAAAAAAAAAAAAAAAAAAAAAAHscOWJPC82eACd+qK9xbXL02jmFvFLpxjxGcWv3N/wBzfq8ndqk68fpCncqlidSMIVZZ56cXLp5XkpY7+eEiI0zVLqKjp84wq05SSUKsdyTbxw+8f4ZO3us6RS1atb3FlU246Ets04uMeE9rXDjjKw/9zT55iYtFknq+h6FR+lpV6NOPFPMKmeZSxxz55fGPuQ15OtL6MV3Ol/UmoU5yz4nSjKWxtfdpRz5pIyl9P6NSqydercdKEVU6n9PpyT7bWuW5dsJZN1jq+nalqcqFLT5f1abglOfgShHdGKjFLCzH1836lliLxHfqK0avXv7Gra37c6EKcpbpc9KSXh2t88vjb5kCd19q11f0lRm4xprlU6a2wX8Lu/u8nCZl9FMWWL6djay0C8V9OSh/Qy4JOX73jCfHfB0TWn6HpH6/S+pUdxGVJTltSp/5k0ud3H+nzA22oTt9NrWUaaarbMvzWyWVj8mdjqc7Wxq2U6SnTqrlP+2S7SX3/wB8It2Jom8z+U/KNTTtKt6On6rTt99NVJtuSnNy+8Yvwrslk5dRhHUbq2/TV6VW5k3Go4p7ZYacXJNLyznjnBH22rw/RRs9RsY1oQzszKUZRz3SlH+37GqeqOF/C7sbWFLp42qKb7f5m+ZN5xn0F0iib/v9WzS7qpda4rGvrka0ZbozpOm4waw+IeXD/GUvMgtMrS0v6ZlqVpjqzrdLfhNwjt3PGezfqY2+v21rqKv7XR4Rnlttzm1znO1do9/ucWl6pKwpSt6lCNSlPG6nLOMrs01ypfdC5FE846KWv6nVuaTqXOZQlxNpbsSwms47fb/1jb9Z3dxca9Uo1qrcacmoLjhNLJxX2oUa0IU7LT4Uowe5YblJv7yly19jPWtTparW/UKxjTm3mclKT3PCXZ8Lt5EainzE2RoAI6gAAAAAAAAAAAAAAAAAAAAAAAAAAAADOhVlQrxrRXMZKS/h5Ju9t9Iur6eoS1ZKE5OexQk6qbeXHH7e/G7OCBBWZpv5T8tdtK9H/Da9k1ar9ii/6kHz4svht5eU+P8AusY6dpNf/EaeqRq7Yy6cFGSk5OLS3JrEUs57sgALs4R6h4lhYPQCOgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADr0izWoalC1lPbGT8UspbYrmcueOIpv+CTnoaqfUULazpSlRqOnUW2Sm1TnKMZeKOU9sm6bkvNZIFNrszOFWpTeadRrCaWG1w+/8MkxPxqJj7C1Q+m7KVtGPU/qPuupF7VV3Kg3FeJJS6WW1yq6/wAphV+mreOmzSliuo+GLnFbpU0pVsRfLXMoLHZ0XnuVffPOd79O/wDz0Xwh1J7t295XnlmcautZ08W+++nrOi5VqFjUapqpim5TU5uLprEk4p5SnKcnTcotR4a7mNnoenVaWLq0lTnOVTFOU5dVKNGjNKCUdspZm2oz2trauXkqn6ivujLrSzHiL3PMfx6fwY9So5bnN5znOXnPr+fuTCernTxaLjS9FsrPfePDknjxT3Z6FCa2pRcXmdRp7nHh9+MnNVstPrapZ2tCzcFX6Ln45SeJz2tLPb8lflKUv3Sb/n/novgKclJSUnldnnt+C4z1ma44sn0/oVG4tJVdRt5qUXLwy3x4VNyWVFOff0TJCX09pNOrKjOLy3UcVumqjUaNGolCOzDlmb8NTa8YWM5Ke7q5c97uJ59dzz8mHUnu3b3nOc5ff1/P3JjVM+2orpiPS10/pyzpQpzuJJ7IVP1C3/tn0Z1YZUN04pOMoNYzmm8dzVd6Xo1CrNyU0qVOnXe2TcKsZ7Y7YSaTS3ygtzWcSqZWYJOs9SeW97578vn8+p5uljG59sfxnOPxnn8lxnqZ08TdlZadLRIVrqcY1KlWpTUpSqLakqXKUU48b23ukvIlKWhae6lWVzYVKMaMqkV1JzxUUadSW54ju8LhHdsTWJxwvWn5eMZ4M5Vqs0lOrJ4W1Zb4Xovt9hNM9IriPiyx0awf1E7VU26f6fqrxTcZPYpZjKMd8qeez257rHBtq/TVqtSp06VKo6bqzVR+Lww6NCostpOP75/uSeFysplVhVqwkpQqNNcJptY/AVaqlJKrLxfu5fi/Pr/Ixq6Z08WOp9P0f1VrGlRqONSrCFRrLSUo27fOOOakv9PQ32ug2V1VhKrZVKW6e3puT5Sr2sNyyk8NVZx8+Y8eaKtG5rxjtjXkl6KTPJVq06nUnVk36tvPHbn88jGrpnTxaI/T9lGyV9RjKtGSqTpxTacoqVCK3bU2um5z37e7j6cnDc6NGOpXFrToSzCjGrCKbk8vpPzjGTSjKTw4ppLnsyEhVqU5KVOo01ymm1j8eh7GrUhV6sajUs53JvOfXPfIimepNVM/FpqfT1rGzqJW0+ooZ8TmknG3p1JJNRcVJTct0am3jCTTN91oel2+o1KUrdKFONV5buFnbhLc9n/T3fBT1WqqMoqrLEv3LL8X59f5MqlzcVI7aleTXo5NkwnrWdPFgjolnL6jqWKT2KnGaxLKbap8xlhOUHue1tLKwdq0DSqN7S8DqU61xUpx8bTioqHDx/dGW6P3WH5op/UmnlTfbHd9v/AU5p5U33z38/X8/cs0z1Irp4s9tpen3Vjb14WWOs1nxVm4p1pQ4ai6f7V/dJPv9s76mj6NGDucR2RhVeVOs6acalGCUns6in45ZiotYcH2ZU4XFeFPpwryUfRSePgx3zUXHe8Pus9xhPTOLelqtvpuzu6VJxq7ZSnGU4xnFtUqknGGIy8Sf/5tSkuevHjjAstHtK9tO6r6TOliMcQnKq0/E02lCMqn25WMp9uyqm+T/ufKx38vT8cL4Nv6u56nU/Uz3NYb3POPyJpq6RXTxO3mj20NCldUraUZxjvbk5rjq7OMx2SjhpYclNSzlcNFcM5VqsodOVWTWd2G3jPrj1+5gapiY9s1TE+oAAVkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGfRrezL4Y6Nb2ZfDAMZuus6Nb2ZfDHRrezL4YAzNZ0a3sy+GOjW9mXwwBmazo1vZl8MdGt7MvhgDM1nRrezL4Y6Nb2ZfDAGZrOjW9mXwx0a3sy+GAMzWdGt7Mvhjo1vZl8MAZms6Nb2ZfDHRrezL4YAzNZ0a3sy+GOjW9mXwwBmazo1vZl8MdGt7MvhgDM1nRrezL4Y6Nb2ZfDAGZrOjW9mXwx0a3sy+GAMzWdGt7Mvhjo1vZl8MAZms6Nb2ZfDHRrezL4YAzNZ0a3sy+GOjW9mXwwBmazo1vZl8MdGt7MvhgDM1nRrezL4Y6Nb2ZfDAGZrOjW9mXwx0a3sy+GAMzWdGt7Mvhjo1vZl8MAZmt//9k='

    useEffect(() => {
        const dataUsers = async () => {
            const responseData = await axios (
                'https://jsonplaceholder.typicode.com/users'
            )
            setData(responseData.data)
            console.log(responseData)
        }
        dataUsers()
    },[])


    return (

        <div className="container mt-4 contentCard">
            <div className="row">
                <div className="col-md-12">
                    <p>Test de récupération de data depuis une fake API (jsonPlaceholder). Le but de ce test est dans un premier temps de traiter les data
                        côté Front en utilisant des reqêtes asynchrone (package npm axios). J'utilise également deux hooks <strong>useState et useEffect</strong> pour l'initiatlisation
                        du state et le lifeCycle du component
                    </p>
                    <p>
                        Dans cet exemple, j'initialise une state vide []. et ensuite j'ajoute la data. 
                        Ici l'exemple est très simple, mais l'idée est surtout de manipuler les hooks
                    </p>
                </div>
            </div>
        <div className="row">
            {dataUsers.map(users => (
                <div className="col-md-6" key={users.id} >
                    <div className="card mt-4">
                        <img src={urlImg} className="card-img-top" alt="logoReact"/>
                            <div className="card-body text-center">
                                <ul>
                                    <li> {users.name} </li>
                                    <li> {users.username} </li>
                                    <li> {users.email} </li>
                                    <li> {users.address.street}  </li>
                                    <li> {users.address.suite}  </li>
                                    <li> {users.address.city}  </li>
                                </ul>
                            </div>
                            <div className="card-footer text-center">
                                <a href="https://jsonplaceholder.typicode.com/users" target="_blanck">
                                <button type="button" className="btn btn-primary">PlaceholderJson</button>
                                </a>
                            </div>
                    </div>
                </div>
            ))}
        </div>
    </div>
        
    )
}
